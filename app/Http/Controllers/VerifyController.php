<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaidList;
use App\Schedule;
use DB;

class VerifyController extends Controller
{
    public function index(){

        return view('verifications.index');
    }

    public function process(Request $request){
        $phoneNo = $request->get('phoneNo');
        //$date = $request->get('date');
        
       // return view('verifications.index')->with('row1', $row1);
        $customer = PaidList::where('tel', $phoneNo)->get();
        
        return view('verifications.index')->with('customer', $customer);
    }
}
