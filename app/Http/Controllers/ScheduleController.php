<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Schedule;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $schedules = Schedule::all();
        return view('schedules.index')->with('schedules', $schedules);
    }

    public function create(){
        return view('schedules.create');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'date' => 'required',
            'seat' => 'required',
            'description' => 'required',
            'characters' => 'required',
        ]);
        
        $schedule = new Schedule([
            'title' => $request->get('title'),
            'date' => $request->get('date'),
            'seat' => $request->get('seat'),
            'description' => $request->get('description'),
            'characters' => $request->get('characters')
        ]);

        $schedule->save();
        return redirect()->route('home');
    }

    public function edit($id){
        $schedule = Schedule::find($id);
        return view('schedules.edit')->with('schedule', $schedule);

    }

    public function update(Request $request, $id){
        $request->validate([
            'title' => 'required',
            'date' => 'required',
            'seat' => 'required',
            'description' => 'required',
            'characters' => 'required',
        ]);

        $schedule = Schedule::find($id);
        $schedule->title = $request->get('title');
        $schedule->date = $request->get('date');
        $schedule->seat = $request->get('seat');
        $schedule->description = $request->get('description');
        $schedule->characters = $request->get('characters');
        $schedule->save();

        return redirect()->route('home');

    }


    public function destroy($id){
        $schedule = Schedule::find($id);
        $schedule->delete();
        return redirect()->route('home');
    }


}
