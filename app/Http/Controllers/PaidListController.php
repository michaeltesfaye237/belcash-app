<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaidList;

class PaidListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){
        $lst = PaidList::all();
        return view('lists.index')->with('lsts', $lst);
    }

    public function showForm(){
        return view('lists.upload');
    }

    public function store(Request $request){
        $upload = $request->file('upload-file');
        $filePath = $upload->getRealPath();

        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);

        while($columns=fgetcsv($file)){
            
            foreach($columns as $key => $value){
                
            }
            $data = array_combine($header, $columns);
            $fullName=$data['fullName'];
            $show_id = $data['showId'];
            $tel=$data['tel'];
            $seatNo=$data['seatNo'];
            $paymentDate=$data['paymentDate'];
            $ticketNo=$data['ticketNo'];
            $list = new PaidList([
                'fullName' => $fullName,
                'show_id' => $show_id,
                'tel' => $tel,
                'seatNo' => $seatNo,
                'paymentDate' => $paymentDate,
                'ticketNo' => $ticketNo,
    
            ]);
            $list->save();
        }
        return redirect()->route('home');
    }
}
