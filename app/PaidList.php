<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaidList extends Model
{
    //
    protected $table="paid_lists";

    public function schedule(){
        return $this->belongsTo(Schedule::class, 'show_id');
    }

    protected $fillable = [
        'fullName',
        'tel',
        'seatNo',
        'paymentDate',
        'ticketNo',
        'show_id'
    ];
    
    
}
