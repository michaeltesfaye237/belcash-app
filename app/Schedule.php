<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table="schedules";

    public function paidList(){
        return $this->hasMany(PaidList::class, 'show_id');
    }
    protected $fillable = [
        'title',
        'date',
        'seat',
        'description',
        'characters'
    ];

    
}
