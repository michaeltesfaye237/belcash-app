

@extends('layouts.app')

@section('content')

<div class="col-sm-9">
    <form action="{{ route('update', $schedule->id) }}" method="PUT" enctype="multipart/form-data">
        
        {{csrf_field()}}
        <div class="form-group row">
            <label for="title" class="col-md-2 col-form-label"> Show Title </label>
            <div class="col-md-10">
                <input type="text" class="form-control"  id="title" name="title" placeholder="Title" value={{ $schedule->title }}>
            </div>
        </div>


        <div class="form-group row">
            <label for="date" class="col-md-2 col-form-label"> Date </label>
            <div class="col-md-10">
                <input type="text" class="form-control"  id="date" name="date" placeholder="Date" value={{ $schedule->date }}>
            </div>
        </div>


        <div class="form-group row">
            <label for="seat" class="col-md-2 col-form-label"> Seat </label>
            <div class="col-md-10">
                <input type="text" class="form-control"  id="seat" name="seat" placeholder="Limit" value={{ $schedule->seat }}>
            </div>
        </div>


        <div class="form-group row">
            <label for="description" class="col-md-2 col-form-label"> Show Description </label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="description" name="description" placeholder="Description" value={{ $schedule->description }}>
            </div>
        </div>

        <div class="form-group row">
            <label for="characters" class="col-md-2 col-form-label"> Characters </label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="characters" name="characters" placeholder="Characters" value={{ $schedule->characters }}>
            </div>
        </div>

        <input type="submit" value="submit">

    </form>
</div>

@endsection