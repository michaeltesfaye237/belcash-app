@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    @can('isAdmin')
                        <p> Admin Access </p>
                    @else
                        <p> User Access </p>
                    @endcan


                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-sm-6 offset-sm-1">
            <h2> Schedules </h2>
    </div>

    <div class="col-sm-2">
        <a class="btn btn-success" href="{{ url('/schedulesForm') }}"> Create Show Schedule</a>
    </div>

    <div class="col-sm-2">
        <a class="btn btn-success" href="{{ url('/uploadList') }}"> Upload List</a>
    </div>
  
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <table class="table table-bordered">
                <tr>
                    <th>Show Title </th>
                    <th>Show Date </th>
                    <th>Seat Limit </th>
                    <th>Description </th>
                    <th>Characters </th>
                    <th width="280px"> Action </th>
                </tr>

                @foreach($schedules as $schedule)
                <tr>
                    <td>{{ $schedule->title }}</td>
                    <td>{{ $schedule->date }}</td>
                    <td>{{ $schedule->seat }}</td>
                    <td>{{ $schedule->description }}</td>
                    <td>{{ $schedule->characters }}</td>
                    <td> 
                        <a class="btn btn-primary" href="{{ route('edit', $schedule->id)}}"> Edit</a>
                        <a class="btn btn-danger" href="{{ route('destroy', $schedule->id) }}"> Delete</a>
                    </td>                   
                </tr>
                @endforeach
            </table> 
        </div>
    </div>
</div>

@endsection
    