@extends('layouts.app')
    
@section('content')
<h1> Upload List </h1>
    <form method="post" action="{{url('/uploadList')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <label for="upload-file">Upload</label>
        <input type="file" name="upload-file">
        <input type="submit" class="btn btn-primary" name="submit" value="Upload">
    </form>
@endsection