@extends('layouts.app')

@section('content')
 
<form action="{{ url('/verifyCustomer') }}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="container">

            <div class="form-group row">
                <label for="phoneNo" class="col-md-2 col-form-label"> Phone No. </label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="phoneNo" name="phoneNo" placeholder="Tel:">
                </div>
            </div>
            <!--
            <div class="form-group row">
                <label for="date" class="col-md-2 col-form-label"> Date </label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="date" name="date" placeholder="Show Date&Time">
                </div>
            </div>
            -->
            <input type="submit" value="Verify" class="offset-sm-2 btn btn-success" >

        </div>
</form>



@endsection