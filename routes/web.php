<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::get('/home', 'ScheduleController@index')->middleware('admin')->name('home');
Route::get('/schedulesForm', 'ScheduleController@create')->middleware('admin');
Route::post('/schedules', 'ScheduleController@store')->middleware('admin');
Route::get('/schedules/{schedule}/edit', 'ScheduleController@edit')->middleware('admin')->name('edit');
Route::put('/schedules/{schedule}', 'ScheduleController@update')->middleware('admin')->name('update');
Route::get('/schedules/{schedule}', 'ScheduleController@destroy')->middleware('admin')->name('destroy');


Route::get('/uploadListIndex', 'PaidListController@index')->middleware('admin');
Route::get('/uploadList', 'PaidListController@showForm')->middleware('admin');
Route::post('/uploadList', 'PaidListController@store')->middleware('admin');

Route::get('/verifyCustomer', 'VerifyController@index')->middleware('user');
Route::post('/verifyCustomer', 'VerifyController@process')->middleware('user');
