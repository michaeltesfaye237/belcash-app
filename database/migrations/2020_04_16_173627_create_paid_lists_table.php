<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaidListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_lists', function (Blueprint $table) {
            $table->id();
            $table->string('fullName', 200);
            $table->bigInteger('show_id')->unsigned();
            $table->bigInteger('tel');
            $table->string('seatNo');
            $table->dateTime('paymentDate');
            $table->string('ticketNo');
            $table->timestamps();
            $table->foreign('show_id')->references('id')->on('schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paid_lists');
    }
}
